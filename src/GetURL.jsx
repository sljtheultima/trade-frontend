export const GetURL =() =>{
    
    let str = window.location.origin;
    
    const searchPattern = 'trade-frontend'
    if(str.includes(searchPattern))
    {
        const replaceWith = "trade-backend"
        return  str.replace(searchPattern,replaceWith)
    }

    return process.env.REACT_APP_BASE_URL
    
  

}
