import React,{Component} from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Header, Icon, Button, Form, Input,Menu, Segment} from 'semantic-ui-react'
import {Chart} from './Chart'
import './App.css';
import pic from './images/img3.png'
import {GetURL} from './GetURL'

class LandingPage extends Component {

    state = {data:[],term:'',searchTerm:null,searchType:null,stocks:[],url:GetURL(),activeItem: 'Search'};


    onFormSubmit = (event) =>{
        event.preventDefault();
        this.state.term!==''? this.getData(`${this.state.url}/trade/ticker/${this.state.term}`) : this.getData()
        
    }
    componentDidMount(){
        this.getData()
        this.getData(`${this.state.url}/getStocks`,"stocks")
        
    }
    componentDidUpdate(prevprops,prevState){
        
        
        if(prevState.searchTerm !== this.state.searchTerm)
        {
                
            this.state.searchTerm !==''? this.getData(`${this.state.url}/trade/state/${this.state.searchTerm}`) : this.getData()
                
        }

        if(prevState.searchType !== this.state.searchType)
        {

            this.state.searchType !==''? this.getData(`${this.state.url}/trade/type/${this.state.searchType}`) : this.getData()

        }

        
    }

    renderLinkedButton =(option,stockItem,buttonColor)=><Link to={ {pathname :'/AddNewTrade',state:{option,stockItem} }}  className="nav-link" ><Button color={buttonColor} renderAs={ "button"}> {option}</Button></Link>
    
    renderStockData= () =>
         this.state.stocks.map((stockItem)=> {
            const {longName,symbol,regularMarketDayLow,regularMarketVolume} = stockItem
            return <tr><td>{longName}</td><td>{symbol}</td><td>{regularMarketDayLow}</td><td>{regularMarketVolume}</td><td>
                {this.renderLinkedButton("BUY",stockItem,"teal")}
                {this.renderLinkedButton("SELL",stockItem,"red")}</td></tr>}
                 )
        
        
    getData = (url = `${this.state.url}/trades`,key = "data") =>{
        fetch(url).then((response)=>{
            response.json().then((data)=>{
                this.setState({[key]:data})

            })
        })

    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    


  render() {
      const { activeItem } = this.state
    
    return (
        
        <div className='wrapper'>
            <div >

                <div class='container'>
                    <img src={pic} class='ui fluid image'/>
                    <div class='centered'>
                        <h1 className='big' class='ui grey inverted huge header' >Trade</h1>
                        <h3 className='ui grey inverted header'>BUY, SELL AND SEARCH DIGITAL ASSETS</h3>
                    </div>
                </div>
                <p></p>
                <div>
                    <Menu attached='top' tabular>
                        <Menu.Item
                            name='Search'
                            active={activeItem === 'Search'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name='Stocks and Trade Submission'
                            active={activeItem === 'Stocks and Trade Submission'}
                            onClick={this.handleItemClick}
                        />
                        <Menu.Item
                            name='Charts'
                            active={activeItem === 'Charts'}
                            onClick={this.handleItemClick}
                        />
                    </Menu>

                    {this.state.activeItem == "Charts" &&
                    <Segment attached='bottom' >

                        <h1 className="ui horizontal divider header">
                            <i className="chart line icon"></i>
                            Charts

                        </h1>
                        <Chart stocksData = {this.state.stocks} />
                    </Segment>}

                    {this.state.activeItem =="Search" &&
                       <Segment attached='bottom' >

                           <h1 className="ui horizontal divider header" >
                               <i className="search icon"></i>
                               Search Trade Records
                           </h1>

                           <form  onSubmit = {this.onFormSubmit}>

                               <div className="ui horizontal segments">

                                   <div className="ui center aligned segment">

                                       <div className="ui blue pointing below label" >By Ticker:</div>
                                       <div>
                                           <Input
                                               icon='search'
                                               type="text"
                                               placeholder="Search tickers..."
                                               value={this.state.term}
                                               onChange={event => this.setState({term: event.target.value})}/>
                                       </div>
                                   </div>
                                   <div className="ui center aligned segment">
                                       <div className="ui blue pointing below label">By State:</div>
                                       <div>
                                           <select className="ui selection dropdown"
                                                   onChange={event => this.setState({searchTerm: event.target.value})}>
                                               <option value="">ALL</option>
                                               <option value="CREATED">CREATED</option>
                                               <option value="FILLED">FILLED</option>
                                               <option value="REJECTED">REJECTED</option>
                                               <option value="PROCESSING">PROCESSING</option>
                                           </select></div>
                                   </div>
                                   <div className="ui center aligned segment">
                                       <div className="ui blue pointing below label">By Type:</div>
                                       <div>
                                           <select className="ui selection dropdown"
                                                   onChange={(event) => this.setState({searchType: event.target.value})}>
                                               <option value="">ALL</option>
                                               <option value="BUY">BUY</option>
                                               <option value="SELL">SELL</option>
                                           </select></div>
                                   </div>
                               </div>

                           </form>

                           <h4 className="ui horizontal divider header">
                               <i className="folder open icon"></i>
                               Search Results
                           </h4>
                           <div className='containerTable'>
                               <table className="ui celled table">
                                   <thead>
                                   <tr>
                                       <th>Ticker</th>
                                       <th>Price (USD)</th>
                                       <th>Amount of shares owned</th>
                                       <th>Total (USD)</th>
                                       <th>Type</th>
                                       <th>Status</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   {this.state.data.map(({price, ticker, quantity, type, state}) => <tr>
                                       <td>{ticker}</td>
                                       <td>{price}</td>
                                       <td>{quantity}</td>
                                       <td>{(quantity * price).toFixed(2)}</td>
                                       <td>{type}</td>
                                       <td>{state}</td>
                                   </tr>)}
                                   </tbody>
                               </table>
                           </div>

                       </Segment>
                    }

                    {this.state.activeItem == "Stocks and Trade Submission" &&
                    <Segment attached='bottom' >

                        <h1 className="ui horizontal divider header">
                            <i className="money bill alternate icon"></i>
                            Stocks
                        </h1>
                        <div>
                            <table className = "ui celled table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Symbol</th>
                                    <th>Price(USD)</th>
                                    <th>Shares volume</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.renderStockData()}
                                </tbody>
                            </table>
                        </div>
                    </Segment>}
                </div>
            </div>
        </div>


    );
  }
}

export default LandingPage;