import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import NewTrade from './AddNewTrade'
import LandingPage from './LandingPage'


const App = () =>{
  return (
      <Router>
    <div className="App">

        <Switch>
            <Route exact path='/' component={LandingPage} />
            <Route path='/AddNewTrade' component={NewTrade} />
        </Switch>


    </div>
      </Router>
  );
}

export default App;
