import React  from 'react'
import {Line,Pie,Bar } from 'react-chartjs-2';

export const Chart = (props) =>{
 

const renderLineStocksData =(stocksInfo )=>{
  if(stocksInfo)
  {
    
    return {
      labels: stocksInfo.map(({shortName})=>shortName),
      datasets:[{
        label : '52 Weeks high average',
        fill:false,
        lineTension:0.5,
        borderColor:'rgba(0,0,0,1)',
        borderWidth:2,
        data: stocksInfo.map(({fiftyTwoWeekHigh}) => fiftyTwoWeekHigh)
      },
      {
        label : '52 Weeks high change % average',
        fill:false,
        lineTension:0.5,
        borderColor:'rgba(255,0,0,1)',
        borderWidth:2,
        data: stocksInfo.map(({fiftyTwoWeekHighChange}) => fiftyTwoWeekHighChange)
      },
      {
        label : '52 Weeks low average',
        fill:false,
        lineTension:0.5,
        borderColor:'rgba(60, 179, 113)',
        borderWidth:2,
        data: stocksInfo.map(({fiftyTwoWeekLow}) => fiftyTwoWeekLow)
      },
      {
        label : '52 Weeks low change % average',
        fill:false,
        lineTension:0.5,
        borderColor:'rgba(0, 0, 255)',
        borderWidth:2,
        data: stocksInfo.map(({fiftyTwoWeekLowChangePercent}) => fiftyTwoWeekLowChangePercent)
      }]
    }
  }
 

}

const renderBarStocksData = (stocksInfo) =>{
  if(stocksInfo){
    return {
      labels: stocksInfo.map(({shortName})=>shortName),
      datasets: [{
        label: "Regular market Price (High)",
        backgroundColor: 'rgba(75,192,192,1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
        data: stocksInfo.map(({regularMarketDayHigh}) => regularMarketDayHigh)
      },
      {
        label: "Regular market Price (Low)",
        backgroundColor: 'rgba(255,0,0,1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
        data: stocksInfo.map(({regularMarketDayLow}) => regularMarketDayLow)
      },
      {
        label: "Regular market Price (Open)",
        backgroundColor: 'rgba(0,0,255,1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
        data: stocksInfo.map(({regularMarketOpen}) => regularMarketOpen)
      }]
    }
  }
}



const renderPieStocksData = (stocksInfo) =>{
  if(stocksInfo){
    return {
      labels: stocksInfo.map(({shortName})=>shortName),
      datasets : [{
        backgroundColor: [
          '#B21F00',
          '#C9DE00',
          '#2FDE00',
          '#00A6B4',
          '#6800B4',
          '#DA344D',
          '#A7C6DA',
          '#EEFCCE',
          '#574D68',
          '#188FA7'

        ],
        hoverBackgroundColor: [
        '#501800',
        '#4B5000',
        '#175000',
        '#003350',
        '#35014F'
        ],
        data:stocksInfo.map(({averageDailyVolume3Month})=>averageDailyVolume3Month)
      }]
    }
  }
}


   
  
    
    return <div>
      <div style = {{width:"1000px"}}>
      <Line
      data={renderLineStocksData(props.stocksData)}
      options={{
        title:{
          display:true,
          text:'Companies Data for a year',
          fontSize:20
        },
        legend:{
          display:true,
          position:'right'
        }
      }}
      />
    </div>
    <div style = {{width : "800px",float : "right" ,marginTop:"50px", marginBottom:"50px"}}>
    <Pie
          data={renderPieStocksData(props.stocksData)}
          options={{
            title:{
              display:true,
              text:'Average volume per day for 3 months ',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
    </div>
      <div style ={{width:"1000px"}}>
      <Bar
          data={renderBarStocksData(props.stocksData)}
          options={{
            title:{
              display:true,
              text:'Companies Data for Regular Market',
              fontSize:20
            },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
      </div>


  
  </div>
  

}