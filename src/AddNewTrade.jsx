import React,{Component} from 'react'
import { Header, Icon,Input , Label, Button, Form, Container, Grid} from 'semantic-ui-react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import {GetURL} from './GetURL'

class NewTrade extends Component{

    state = { 
     
        ticker : this.props.location.state.stockItem.symbol,
        price :this.props.location.state.stockItem.regularMarketDayLow,
        quantity :0,
        type : this.props.location.state.option,
        url : GetURL()

    }
    insertData = ()=>{
        fetch(`${this.state.url}/addNewTrade`, {
            method: 'post',
            mode: 'cors', 
            headers: {
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(this.state)
          }).then((response)=>{response.json().then((data)=>{console.log(data)})})
      
    }
    onSubmit = (event) =>{
        event.preventDefault();
        this.insertData(JSON.stringify(this.state))
        alert("Submitted!")
        this.props.history.push('/')
    }



    render(){
        console.log(this.props)
        return <div>
            <p></p>
            <Header as='h2' icon textAlign={'center'}>
                <Icon name='file alternate outline'  circular/>
                <Header.Content>New Trade Submission - {this.props.location.state.option}</Header.Content>
            </Header>

    <Container>
        <Grid centered>
            <Form >
                <p></p>

                <Form.Field
                    control={Input}
                    label='Ticker'
                    placeholder='Name of Ticker'
                    value = {this.props.location.state.stockItem.symbol}
                    onChange = { (e)=>this.setState({ticker :e.target.value })}
                />
                <Form.Field
                    control={Input}
                    label= "Price ($)"
                    type = "number"
                    step = "0.01"
                    placeholder = "Price of trade"
                    value ={this.props.location.state.stockItem.regularMarketDayLow}
                    onChange = { (e)=>this.setState({price :e.target.value })}
                />
                <Form.Field
                    control={Input}
                    label= "Quantity"
                    type = "number"
                    step = "0.01"
                    placeholder = "Quantity of trade"
                    onChange = { (e)=>this.setState({quantity :e.target.value })}
                />
                <p></p>

                <div class = "fields">

                <Link to={ {pathname :'/' }}  className="nav-link" ><Button color='grey' renderAs={ "button"}>Cancel</Button></Link>
                    <Form.Field control={Button} onClick = {this.onSubmit} color='blue'>
                        Submit
                    </Form.Field>
                </div>
            </Form>
        </Grid>
    </Container>
        </div>
    }
}

export default NewTrade