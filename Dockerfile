FROM node:alpine AS staging

RUN mkdir /app

WORKDIR /app
COPY package.json /app

RUN sh -c 'echo REACT_APP_BASE_URL=http://trade-backend:8081 > .env'

RUN npm install
COPY . /app

RUN npm run build

RUN chmod 777 -R /app/build

# FROM nginx:stable-alpine
FROM nginxinc/nginx-unprivileged 

COPY --from=staging /app/build /usr/share/nginx/html
EXPOSE 8080
